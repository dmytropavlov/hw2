const express = require('express');
const router = express.Router();

const {
  getNotesByUserId,
  getNoteByIdForUser,
  addNoteToUser,
  changeNoteByIdForUser,
  checkNoteByIdForUser,
  deleteNoteByIdForUser
} = require('../services/notesService');

const {
  asyncWrapper
} = require('../utils/apiUtils');

const {
  InvalidRequestError
} = require('../utils/errors');


router.get('/', asyncWrapper(async (req, res) => {
  const { userId } = req.user;
  
  let limit = +req.query.limit || 0;
  let offset = +req.query.offset || 0;
  
  const length = await getNotesByUserId(userId, 0, 0);
  
  const notes = await getNotesByUserId(userId, limit, offset);
  res.json({ "offset": offset, "limit": limit, "count": length.length, notes });
}));



router.get('/:id', asyncWrapper(async (req, res) => {
  const { userId } = req.user;
  const { id } = req.params;

  const note = await getNoteByIdForUser(id, userId);

  if (!note) {
    throw new InvalidRequestError('There is no note with such id!');
  }

  res.json({ note });
}));

router.post('/', asyncWrapper(async (req, res) => {
  const { userId } = req.user;

  await addNoteToUser(userId, req.body);

  res.json({ message: "Success" });
}));

router.put('/:id', asyncWrapper(async (req, res) => {
  const { userId } = req.user;
  const { id } = req.params;
  const data = req.body;

  const note = await changeNoteByIdForUser(id, userId, data);

  if (!note) {
    throw new InvalidRequestError('There is no note with such id!');
  }

  res.json({ message: "Success" });
}));

router.patch('/:id', asyncWrapper(async (req, res) => {
  const { userId } = req.user;
  const { id } = req.params;


  const note = await checkNoteByIdForUser(id, userId);

  if (!note) {
    throw new InvalidRequestError('There is no note with such id!');
  }

  res.json({ message: "Success" });
}));

router.delete('/:id', asyncWrapper(async (req, res) => {
  const { userId } = req.user;
  const { id } = req.params;

  const note = await deleteNoteByIdForUser(id, userId);

  if (!note) {
    throw new InvalidRequestError('There is no note with such id!');
  }

  res.json({ message: "Success" });
}));

module.exports = {
  notesRouter: router
}