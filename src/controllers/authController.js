const express = require('express');
const router = express.Router();

const {
    register,
    login
} = require('../services/authService');

const {
    asyncWrapper
} = require('../utils/apiUtils');
const {
    registerValidator
} = require('../middlewares/validationMidlleware');

router.post('/register', registerValidator, asyncWrapper(async (req, res) => {
    const {
        password,
        username
    } = req.body;

    await register({password, username});

    res.json({message: 'Success'});
}));

router.post('/login', asyncWrapper(async (req, res) => {
    const {
        username,
        password
    } = req.body;

    const token = await login({username, password});

    res.json({jwt_token: `${token}`, message: 'Success'});
}));

module.exports = {
    authRouter: router
}