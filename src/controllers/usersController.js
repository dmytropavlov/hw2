const express = require('express');
const router = express.Router();

const {
  getUserById,
  removeUserById,
  changePassword
} = require('../services/usersService');

const {
  asyncWrapper
} = require('../utils/apiUtils');

const {
  InvalidRequestError
} = require('../utils/errors');


router.get('/me', asyncWrapper(async (req, res) => {
  const { userId } = req.user;

  const oldUser = await getUserById(userId);

  if (!oldUser) {
    throw new InvalidRequestError('There is no profile!');
  }

  const { createdAt, _id, username } = oldUser;

  const user = {};
  user._id = _id;
  user.username = username;
  user.createdAt = createdAt;


  res.json({ user });
}));

router.patch('/me', asyncWrapper(async (req, res) => {
  const { userId } = req.user;
  const {
    oldPassword,
    newPassword
  } = req.body;


  const oldUser = await getUserById(userId);

  if (!oldUser) {
    throw new InvalidRequestError('There is no profile!');
  }

  const { createdAt, _id, username } = oldUser;


  const user = {};
  user._id = _id;
  user.username = username;
  user.createdAt = createdAt;
  
  await changePassword(user._id, oldPassword, newPassword)

  res.json({"message":"Success"});
}));

router.delete('/me', asyncWrapper(async (req, res) => {
  const { userId } = req.user;

  const user = await removeUserById(userId);

  if (!user) {
    throw new InvalidRequestError('There is no profile!');
  }

  res.json("message: Success");
}));

module.exports = {
  usersRouter: router
}