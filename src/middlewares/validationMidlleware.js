const Joi = require('joi');

const registerValidator = async (req, res, next) => {
    const schema = Joi.object({
        username: Joi.string()
            .alphanum()
            .min(3)
            .max(15)
            .required(),
        password: Joi.string()
            .min(6)
            .max(20)
    });

    try {
        await schema.validateAsync(req.body);
        next();
    }
    catch (err) {
        next(err);
    }
};

module.exports = {
    registerValidator,
}