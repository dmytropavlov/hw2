const {User} = require('../models/userModel');
const bcrypt = require('bcrypt');

const getUserById = async (userId) => {
  const user = await User.findOne({_id: userId});
  return user;
}

const removeUserById = async (userId) => {
  const user = await User.findOneAndRemove({_id: userId});
  return user;
}

const changePassword = async (userId, password, newPassword) => {
  const user = await User.findOne({_id: userId});

  if (!user) {
   
      throw new Error('Invalid userId');
  }

  if (!(await bcrypt.compare(password, user.password))) {
      throw new Error('Invalid password');
  }

  let newPass = await bcrypt.hash(newPassword, 10);

  await User.updateOne({_id: userId}, { $set: { password: newPass } });
}

module.exports = {
  getUserById,
  removeUserById,
  changePassword
}