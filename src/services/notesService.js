const { Note } = require('../models/noteModel');

const getNotesByUserId = async (userId, lim, offset) => {
  const notes = await Note.find({ userId }).limit(lim).skip(offset);
  return notes;
}

const getNoteByIdForUser = async (noteId, userId) => {
  const note = await Note.findOne({ _id: noteId, userId });
  return note;
}

const addNoteToUser = async (userId, notePayload) => {
  const note = new Note({ ...notePayload, userId });
  await note.save();
}

const changeNoteByIdForUser = async (noteId, userId, data) => {
  const note = await Note.findOne({ _id: noteId, userId });
  if (!note)
    return false;
  else {
    await Note.findOneAndUpdate({ _id: noteId, userId }, { $set: data });
    return true;
  }
  
}

const checkNoteByIdForUser = async (noteId, userId) => {
  const note = await Note.findOne({ _id: noteId, userId });
  if (!note)
    return false;
  else {
    await Note.updateOne({ _id: noteId, userId }, { $set: { completed: !note.completed } });
    return true;
  }

}

const deleteNoteByIdForUser = async (noteId, userId) => {
  const note = await Note.findOne({ _id: noteId, userId });
  if (!note)
    return false;
  else {
    await Note.findOneAndRemove({ _id: noteId, userId });
    return true;
  }


}


module.exports = {
  getNotesByUserId,
  getNoteByIdForUser,
  addNoteToUser,
  deleteNoteByIdForUser,
  changeNoteByIdForUser,
  checkNoteByIdForUser
}