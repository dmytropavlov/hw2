const mongoose = require('mongoose');

const User = mongoose.model('User', {
    password: {
        type: String,
        required: true,
    },

    username: {
        type: String,
        unique: true
    },

    createdAt: {
        type: Date,
        default: Date.now()
    }
});

module.exports = { User };
