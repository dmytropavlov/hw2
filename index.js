const express = require('express');
const morgan = require('morgan')
const mongoose = require('mongoose');
const app = express();
const port = process.env.PORT || 8080;


const {usersRouter} = require('./src/controllers/usersController');
const {notesRouter} = require('./src/controllers/notesController');
const {authRouter} = require('./src/controllers/authController'); 
const {authMiddleware} = require('./src/middlewares/authMiddleware'); 
const {NodeCourseError} = require('./src/utils/errors'); 
 
app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/users', [authMiddleware], usersRouter);
app.use('/api/notes', [authMiddleware], notesRouter);

app.use((req, res, next) => {
    res.status(404).json({message: 'Not found'})
});

app.use((err, req, res, next) => {
    if (err instanceof NodeCourseError) {
        return res.status(err.status).json({message: err.message});
    }
    res.status(500).json({message: err.message});
});

const start = async () => {
    try {
        await mongoose.connect('mongodb+srv://admin:root@cluster0.klsta.mongodb.net/myFirstDatabase?retryWrites=true&w=majority', {
            useNewUrlParser: true, useUnifiedTopology: true
        });
    
        app.listen(port);
    } catch (err) {
        console.error(`Error on server startup: ${err.message}`);
    }
}

start();